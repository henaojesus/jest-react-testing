const randomStrings = require('../index');

/* Describir bloque de tests */
describe('Probar funcionalidades', () => {
  test('Probar función randomStrings', () => {
    expect(typeof (randomStrings())).toBe('string');
  });
  test('Comprobar que no existe una ciudad', () => {
    expect(randomStrings()).not.toMatch(/Cordoba/);
  });
})