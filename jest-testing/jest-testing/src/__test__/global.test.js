const text = "Hola Mundo";
const fruits = ['manzana', 'melon', 'banana'];

/* String Test */
test('Debe contener un texto', () => {
  expect(text).toMatch(/Mundo/);
});

/* Array Test */
test('¿Tenemos una banana?', () => {
  expect(fruits).toContain('banana');
});

/* Number Test */
test('Mayor que', () => {
  expect(10).toBeGreaterThan(9);
});

/* If value is Boolean Test */
test('Verdadero', () => {
  expect(true).toBeTruthy();
});

/* Testing Callback */
const reverseString = (str, callback) => {
  callback(str.split("").reverse().join(""));
};
test('Probar un Callback', () => {
  reverseString('Hola', (str) => {
    expect(str).toBe('aloH');
  });
});

/* Testing Promise */
const reverseString2 = str => {
  return new Promise((resolve, reject) => {
    if (!str) {
      reject(Error('Error'))
    }
    resolve(str.split("").reverse().join(""))
  });
}
test('Probar Promesa', () => {
  return reverseString2('Hola')
    .then(string => {
      expect(string).toBe('aloH');
    });
});
test('Probar async/await', async () => {
  const string = await reverseString2('hola');
  expect(string).toBe('aloh');
});

/* afterEach(() => console.log('Despues de cada prueba'));
afterAll(() => console.log('Despues de todas las pruebas'));

beforeEach(() => console.log('Antes de cada prueba'));
beforeAll(() => console.log('Antes de todas las pruebas')); */